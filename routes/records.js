// Author: Umut Cem Soyulmaz

const express = require('express');

// importing the RecordController which handles the API Gateway 
// functionalities as a controller.
const RecordController = require('../controllers/RecordController');

const router = express.Router();

// declaration of the function which will be used in 
// RecordController.js file
router.post('/', RecordController.postFunction);

module.exports = router;