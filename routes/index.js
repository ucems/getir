// Author: Umut Cem Soyulmaz

const express = require('express');
var bodyParser = require('body-parser');

// importing the records.js file for /records route
const records = require('./records');

const router = express.Router();

router.use(express.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json())

// declaration about using the /records route
router.use('/records', records);

module.exports = router;