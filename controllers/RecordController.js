// Author : Umut Cem Soyulmaz

const Record = require('../models/Record');

// This function is responsible for all the properties which
// was given in the specification. These properties are:
//    1) Filtering the results based on the date interval inputs
//    2) Filtering the results based on total count interval inputs
//    3) Returning these final results as a json object
function postFunction(req,res){

  // Checking whether there is a missing input parameter or not
  if(req.body.startDate != null && 
    req.body.endDate != null &&
    req.body.minCount != null &&
    req.body.maxCount != null){

      // Getting all the available records to filter them later
      let filter = {}
      Record.find(filter,function(err, records){

        // if there is an error, returns an error
        if (err) console.log(err);

        // if getting the records from the db is all good, do the following:
        else{

          // the final json object called "response" for returning as the response payload.
          response = {};
          response['code'] = 0;
          response['msg'] = "Success";

          // records is an inner array inside the "response" json object.
          // it stores the filtered records in itself.
          response['records'] = [];

          // iterating all the records one by one for filtering them properly.
          for(var i = 0; i < records.length; i++){

            // creating an json object for storing the individual properties of records.
            item = {};
            item['key'] = records[i].key;
            item['createdAt'] = records[i].createdAt;
            
            // a variable which keeps track of the total count value of a single record
            // by using the for loop below
            var totalCount = 0;
            for(var j = 0; j < records[i].counts.length; j++){
              totalCount = totalCount + records[i].counts[j];
            }

            item['totalCount'] = totalCount;

            // converting the given date values to a common date format 
            // by using getTime() function and storing these values inside
            // the "from" & "to" & "creationDate" variables.
            var from = new Date(req.body.startDate).getTime();
            var to = new Date(req.body.endDate).getTime();
            var creationDate = new Date(records[i].createdAt).getTime();

            // checking whether the given records is matching with the
            // properties that was set by the user inside the request payload
            if(totalCount >= req.body.minCount && 
              totalCount <= req.body.maxCount &&
              creationDate >= from &&
              creationDate <= to){

              // if the records passes the filtering process properly, adds it to the response.
              response['records'].push(item);
            }

          }

          // returns the response json object.
          res.json(response);
        }

      });

    }

    // If there is a missing input parameter, returns an error.
    else{
      res.json({"code": -1, "msg": "Missing body parameters! Please check the request body."});
    }

  
}

module.exports = { postFunction };