// Author : Umut Cem Soyulmaz

const server = require('./server.js');

// Declaring the port number
const port = process.env.PORT || 3001;

// Listening and checking the port functionality
server.listen(port, () => console.log(`Server up and running on port ${port} !`));
