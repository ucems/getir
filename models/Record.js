const mongoose = require('mongoose');

// Schema for declaring the object properties of
// the given collection
const RecordSchema = new mongoose.Schema({
    key: String,
    createdAt: Date,
    counts: [Number, Number],
    value: String,
});

// using the schema above as a mongoose model
var Record = mongoose.model('Record', RecordSchema);
module.exports = Record;