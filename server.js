// Author : Umut Cem Soyulmaz

const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');

// Declaration in order to use express.js
const app = express();

// Global header declarations for access controls
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// declaration of routes folder which contains all the possible routes.
app.use('/', routes);

app.use(express.json())


// MongoDB Uri which was provided by Getir.com
const dbURI = `mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true`;

mongoose.Promise = global.Promise;

// setting the options of web server
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

// Checking whether the connection is successful with the database
mongoose.connect(dbURI, options).then(
  () => {
      console.log("Database Connection Established!");
  },
  err => {
      console.log("Error connecting Database instance due to: ", err);
  }
);

module.exports=app;