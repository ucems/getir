process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Record = require('../models/Record');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
var should = require('chai').should();

chai.use(chaiHttp);

// Testing the request with 4 required parameters
describe('/POST record', () => {
    it('it should filter the record results', (done) => {

      let record = {
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        minCount: 2000,
        maxCount: 3000
      }

      chai.request(server)
          .post('/records')
          .send(record)
          .end((err, res) => {
                res.body.records.should.be.a('array');
                res.body.code.should.be.eql(0);
            done();
          });
    });
});

// Testing the request without maxCount parameter
describe('/POST record (missing maxCount)', () => {
    it('it should return an error', (done) => {

      let record = {
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        minCount: 2000
      }

      chai.request(server)
          .post('/records')
          .send(record)
          .end((err, res) => {
                res.body.code.should.be.eql(-1);
            done();
          });
    });
});

// Testing the request without minCount parameter
describe('/POST record (missing minCount)', () => {
    it('it should return an error', (done) => {

      let record = {
        startDate: "2016-01-26",
        endDate: "2018-02-02",
        maxCount: 2000
      }

      chai.request(server)
          .post('/records')
          .send(record)
          .end((err, res) => {
                res.body.code.should.be.eql(-1);
            done();
          });
    });
});

// Testing the request without startDate parameter
describe('/POST record (missing startDate)', () => {
    it('it should return an error', (done) => {

      let record = {
        endDate: "2018-02-02",
        minCount: 1000,
        maxCount: 2000
      }

      chai.request(server)
          .post('/records')
          .send(record)
          .end((err, res) => {
                res.body.code.should.be.eql(-1);
            done();
          });
    });
});

// Testing the request without endDate parameter
describe('/POST record (missing endDate)', () => {
    it('it should return an error', (done) => {

      let record = {
        startDate: "2016-01-26",
        minCount: 1000,
        maxCount: 2000
      }

      chai.request(server)
          .post('/records')
          .send(record)
          .end((err, res) => {
                res.body.code.should.be.eql(-1);
            done();
          });
    });
});

// Testing the request with an empty payload set
describe('/POST record (empty request payload)', () => {
    it('it should return an error', (done) => {

      chai.request(server)
          .post('/records')
          .end((err, res) => {
                res.body.code.should.be.eql(-1);
            done();
          });
    });
});
